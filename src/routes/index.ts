/**
 * Root Router
 * Redirects to Routers
 */

import express, { Request, Response } from 'express';
import helloRouter from './HelloRouter';
import { LogInfo } from '../../src/utils/logger';

// Server instance
const server = express();

// Router instance
let rootRouter = express.Router();

// Activate for request to http://localhost:800/api

// GET: http://localhost:800/api/
rootRouter.get('/', (_req: Request, res: Response) => {
  LogInfo('GET: http://localhost:800/api/');
  // Send hello World
  res.send(
    'Welcome to API Restful: Express + Nodemon + Jest + TS + Swagger + Mongoose'
  );
});

// Redirections to routers & controllers
server.use('/', rootRouter); // http://localhost:800/api
server.use('/hello', helloRouter); // http://localhost:800/api/hello -> HelloRouter
// Add more routes to the app

export default server;
