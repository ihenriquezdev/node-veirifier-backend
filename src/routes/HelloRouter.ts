import express, { Request, Response } from 'express';
import { HelloController } from '../controller/HelloController';
import { LogInfo } from '../utils/logger';
import { BasicResponse } from '../controller/types/index';

// Router from express
let helloRouter = express.Router();

//  http://localhost:800/api/hello?name=Ignacio
helloRouter.route('/').get(async (req: Request, res: Response) => {
  // GET
  const name: any = req?.query?.name;
  LogInfo(`Query Param ${name}`);
  // Controller Instance to execute method
  const controller: HelloController = new HelloController();
  // Obtain Response
  const response: BasicResponse = await controller.getMessage(name);
  // Send to client the response
  return res.send(response);
});

// Export Hello Router
export default helloRouter;
