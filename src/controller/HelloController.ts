import { BasicResponse } from './types';
import { IHelloController } from './interfaces';
import { LogSuccsess } from '../../src/utils/logger';

export class HelloController implements IHelloController {
  public async getMessage(name?: string | undefined): Promise<BasicResponse> {
    LogSuccsess('[/api/hello] Get Request');
    return {
      message: `Hello ${name || 'world!'}`
    };
  }
}
