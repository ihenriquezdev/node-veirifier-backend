import dotenv from 'dotenv';
import server from './src/server';
import { LogSuccsess, LogError } from './src/utils/logger';

//* Configuration the .env file
dotenv.config();

const port = process.env.PORT || 8000;

//* Execute SERVER
server.listen(port, () => {
  LogSuccsess(`[Server on]: Running in http://localhost${port}/api`);
});

//* Control SERVER ERROR
server.on('error', (error) => {
  LogError(`[Server error]: ${error}`);
});
