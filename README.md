# Code Node Express
## _Task 1 | Open Bootcamp - MERN_

## Installed Dependencies

| Dependency | Purpose |
| ------ | ------ |
| express | Allows create and start the server
| dotevn | Allows setting environment variables
| nodemon | Allows to restart the application when changes are made
| typescript | Allows you to configure a typescript project
| concurrently | Allows you to execute commands concurrently
| webpack | Next Lecture
| webpack-cli | Next Lecture
| webpack-node-externals | Next Lecture
| webpack-shell-plugin | Next Lecture
| eslint | Allows you to follow rules at the programming level
| jest | Allows unit and integration tests
| supertest | Next Lecture
| ts-node | Allows to perform the test on the ts file and not the transpiled js

## Scripts
| Script | Purpose |
| ------ | ------ |
| build | Transpiles code from js to ts
| start | run the transpiled solution from the project build
| dev | Transpiles ts to js and concurrently executes the transpiled solution
| test | Run the Jest test suite
| serve:coverage | Run the tests with Jest and serve the Coverage at the web level